import os
import constants

#
#   Copyright (c) 2021 - Yaolinux Teams
#
#  This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
#  USA.
#

def packages_installed():
    for i in os.listdir(constants.INSTALLED_DB_PACKAGE):
        open_package_meta = open(constants.INSTALLED_DB_PACKAGE + "/" + i + "/.PACKMETA", 'r')
        package_meta = open_package_meta.read().strip()
        metadata = package_meta.split(";")
        print(metadata[0] + " - " + metadata[1])


def isInstalled(name):
    if name in os.listdir(constants.INSTALLED_DB_PACKAGE):
        return True
    else:
        return False

