#!/usr/bin/python3

#
#   Copyright (c) 2021 - Yaolinux Teams
#
#  This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
#  USA.
#

import os
import zstandard as zstd
import pathlib
import tarfile

class Archive(object):
    def __init__(self, archive_path):
        self.archive_path = archive_path

    def unpack(self, dest_dir):
        self.dest_dir = dest_dir

        if not (os.path.exists(self.dest_dir)):
            os.makedirs(self.dest_dir)

class ArchiveTar(Archive):
    def __init__(self,  archive_path):
        self.archive_path = archive_path

    def uncompress(self, dest_dir):
        self.dest_dir = dest_dir

        print(self.archive_path)

        if tarfile.is_tarfile(self.archive_path):
            tar = tarfile.open(self.archive_path)
            tar.extractall(self.dest_dir)

class ArchiveZstd(Archive):
    def __init__(self, archive_path):
        super(ArchiveZstd, self).__init__(archive_path)

    def unpack(self, dest_dir):
        super(ArchiveZstd, self).unpack(dest_dir)
        input_file = pathlib.Path(self.archive_path)
        with open(input_file, 'rb') as compressed:
            decomp = zstd.ZstdDecompressor()
            output_path = pathlib.Path(self.dest_dir) / input_file.stem
            with open(output_path, 'wb') as destination:
                decomp.copy_stream(compressed, destination)

        return output_path