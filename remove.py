#!/usr/bin/python3

#
#   Copyright (c) 2021 - Yaolinux Teams
#
#  This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
#  USA.
#

import constants
import os
import shutil

class Remove(object):
    def __init__(self, name=None):
        self.package_name = name

    def pkg_remove(self):
        if self.package_name in os.listdir(constants.INSTALLED_DB_PACKAGE):
            packtree_file = open(constants.INSTALLED_DB_PACKAGE + "/" + self.package_name + "/.PACKTREE", 'r')
            for line in packtree_file.readlines():
                current_line = "/" + line.strip()
                if os.path.isfile(current_line):
                    os.remove(current_line)
                elif os.path.islink(current_line):
                    os.unlink(current_line)
                elif os.path.isdir(current_line):
                    if len(os.listdir(current_line)) == 0:
                        os.rmdir(current_line)
            shutil.rmtree(constants.INSTALLED_DB_PACKAGE + "/" + self.package_name)
            return True
        else:
            return False


