#!/usr/bin/python3
import os
import sys
import shutil


import remove as yamu_remove
import archives as yamu_archve
import constants


#
#   Copyright (c) 2021 - Yaolinux Teams
#
#  This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
#  USA.
#

class Install(object):
    def __init__(self, name):
        self.pkg_name = name

    def install(self):
        out_package = yamu_archve.ArchiveZstd(self.pkg_name).unpack(constants.UNCOMPRESS_TMP)
        yamu_archve.ArchiveTar(out_package).uncompress(constants.ROOT_FILE)
        pkg_name = self.__getName()
        if os.path.exists(constants.INSTALLED_DB_PACKAGE + "/" + pkg_name):
            os.rmdir(constants.INSTALLED_DB_PACKAGE + "/" + pkg_name)

        os.makedirs(constants.INSTALLED_DB_PACKAGE + "/" + pkg_name)
        shutil.move(constants.ROOT_FILE + ".PACKTREE", constants.INSTALLED_DB_PACKAGE + "/" + pkg_name)
        shutil.move(constants.ROOT_FILE + ".PACKMETA", constants.INSTALLED_DB_PACKAGE + "/" + pkg_name)
        if os.path.isfile(constants.ROOT_FILE + ".install"):
            shutil.move(constants.ROOT_FILE + ".install", constants.INSTALLED_DB_PACKAGE + "/" + pkg_name)


        os.system("/sbin/ldconfig -r /")


    def __getMetaData(self):
        path_packmeta = constants.ROOT_FILE + ".PACKMETA"
        read_pm = open(path_packmeta, 'r')
        metadata = read_pm.read().strip().split(';')

        return metadata

    def __getName(self):
        metadata = self.__getMetaData()

        return metadata[0]
