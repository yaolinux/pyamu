#!/usr/bin/python3
import os
import constants
import package_utils as yamu_package_utils

#
#   Copyright (c) 2021 - Yaolinux Teams
#
#  This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
#  USA.
#

class Info(object):
    def __init__(self, name):
        self.package_name = name
        self.metadata = None

    def getMetadata(self):
        if yamu_package_utils.isInstalled(self.package_name):
            open_package_meta = open(constants.INSTALLED_DB_PACKAGE + "/" + self.package_name + "/.PACKMETA", 'r')
            package_meta = open_package_meta.read().strip()
            self.metadata = package_meta.split(";")

            return self.metadata

    def getVersion(self):
        return self.metadata[1]

    def getRelease(self):
        return self.metadata[2]

    def getRuntimeDeps(self):
        return self.metadata[3]

    def getDescription(self):
        return self.metadata[4]