#!/usr/bin/python3

#
#   Copyright (c) 2021 - Yaolinux Teams
#
#  This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
#  USA.
#

import sys
import constants
import shutil
import os

#Yamu
import remove as yamu_remove
import install as yamu_install
import info as yamu_info
import package_utils as yamu_package_utils
import archives as yamu_archve


yamu_function = sys.argv[1]
if sys.argv[2] != " ":
    package = sys.argv[2]

if not os.path.exists(constants.INSTALLED_DB_PACKAGE):
    os.makedirs(constants.INSTALLED_DB_PACKAGE)

if yamu_function == "install" :
    if package != " ":
        yamu_install.Install(package).install()
elif yamu_function == "remove":
    if package != " ":
        if yamu_package_utils.isInstalled(package):
            if yamu_remove.Remove(package).pkg_remove():
                print("Paquet supprimé avec succès")
    else:
        print("Merci de renseigner un paquet")

elif yamu_function == "list":
    yamu_package_utils.packages_installed()

elif yamu_function == "info":
    package_info = yamu_info.Info(package)
    package_info.getMetadata()
    print("Information du paquet " + package + " : ")
    print("Description : " + package_info.getDescription())
    print("Version : " + package_info.getVersion())
    print("Release : " + package_info.getRelease())
    print("Deps runtime : " + package_info.getRuntimeDeps())